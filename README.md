# Godot voice effects test

the curent version of this project is made with [Godot](https://godotengine.org) ``v3.5.1.stable.official [6fed1ffa3]``.

![Screenshot](.readme-assets/screenshot.png)

this is a small project i made to test Godot's audio effects for voice changing.

## Audio Effects

this Desktop app can play the built-in specially-recorded voice clip of me saying gibberish
with 8 different effect chains applied in realtime via Godot's audio buses:

### No effects

[▶ Hear sample](.readme-assets/recordings/no_effects.ogg)

the voice clip, exactly as i recorded it

### Alien

[▶ Hear sample](.readme-assets/recordings/alien.ogg)

effects applied to make the voice sound like an alien in a Sci-Fi movie:
- low-pass filter
- phaser
  to make the voice sound more slimey
- pitch shift (downwards)
  to make the voice sound deeper and more menacing

### Chipmunk

[▶ Hear sample](.readme-assets/recordings/chipmunk.ogg)

effect applied to make the voice sound like an anthropomorphic chipmunk:
- pitch-shift (upwards)

### Demon

[▶ Hear sample](.readme-assets/recordings/demon.ogg)

effect applied to make the voice sound demonic:
- pitch shift (downwards)

### Muffled

[▶ Hear sample](.readme-assets/recordings/muffled.ogg)

effects applied to make the voice sound muffled, as if recorded from a low-quality microphone:
- low-pass filter
- equalizer (boost low frequencies)

### Robot

[▶ Hear sample](.readme-assets/recordings/robot.ogg)

effects applied to make the voice sound robotic:
- pitch shift (downwards)
  to degrade the sound quality
- pitch shift (upwards)
  to restore the original pitch
- chorus

### Squidman

[▶ Hear sample](.readme-assets/recordings/squidman.ogg)

effects applied to make the voice sound like an anthropomorphic squid:
- low-pass filter
- pitch shift (slightly upwards)
- phaser
  to make the voice sound liquid

### Telephone

[▶ Hear sample](.readme-assets/recordings/telephone.ogg)

effects applied to make the voice sound like you're hearing it from the other end on a low-quality phone call:
- high-pass filter
- equalizer (boost highest frequencies)

## LICENSES

- the code is licensed under [MIT License](https://codeberg.org/Sosasees/mit-license/raw/branch/2022/LICENSE)
- the voiceclip by Sosasees is marked with [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0)
